# -*- coding: utf-8 -*-
{
    'name': 'Custom MRP Production',
    'version': '14.0.1.0',
    'summary': 'Customizations for MRP Production',
    'sequence': -90,
    'description': """Customizations for MRP Production""",
    'category': 'Manufacturing',
    'website': 'https://www.google.com',
    'depends': ["mrp"],
    'data': [
        'security/ir.model.access.csv',
        'wizard/mrp_production_split_wizard_view.xml',
        'views/mrp_production_view_inherit.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
