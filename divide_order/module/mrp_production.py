from odoo import models, fields, api


class MrpProductionCustom(models.Model):
    _inherit = "mrp.production"

    def divide_order_button(self):
        result = {
            "name": "Split Order",
            "view_mode": "form",
            "type": "ir.actions.act_window",
            "res_model": "mrp.production.split.wizard",
            "target": "new",
            "context": {
                "default_order_name": self.name,
                "default_order_qty": self.product_qty,
                "default_production_id": self.id
            },
        }
        return result
