from odoo import models, fields, api


class MrpProductionSplitWizard(models.TransientModel):
    _name = "mrp.production.split.wizard"
    _description = "MRP Production Split Wizard"

    order_name = fields.Char(string="Order name", readonly=True)
    order_qty = fields.Integer(string="Quantity", readonly=True)
    production_id = fields.Many2one(comodel_name="mrp.production", string="Production Order")
    order_count = fields.Integer(string="Number of orders", default=1)
    child_order_ids = fields.One2many(comodel_name="mrp.production.child.order", inverse_name="wizard_id",
                                      string="Child Orders")

    @api.onchange("order_count")
    def onchange_order_count(self):
        total_quantity = self.order_qty
        if self.order_count > 0:
            quantity_per_order = total_quantity // self.order_count
            remainder = total_quantity % self.order_count

            child_orders = []
            counter = 1
            self.child_order_ids = False
            while self.order_count > 0:
                qty = quantity_per_order + (1 if remainder > 0 else 0)
                child_orders.append((0, 0, {
                    'child_order_number': counter,
                    'quantity': qty,
                }))
                counter += 1
                remainder -= 1
                self.order_count -= 1

            self.child_order_ids = child_orders

    def create_child_orders(self):
        child_orders = []
        original_prefix = 1
        for order in self.child_order_ids:
            child_order = self.env['mrp.production'].create({
                'name': f"{self.order_name} - Child {original_prefix}",
                'product_id': self.production_id.product_id.id,
                'product_qty': order.quantity,
                'product_uom_id': self.production_id.product_uom_id.id,
                'date_planned_start': self.production_id.date_planned_start,
                'date_deadline': self.production_id.date_deadline,
                'origin': self.production_id.name,
                'state': 'draft',
            })
            child_orders.append(child_order)
            original_prefix += 1
        return child_orders

    def split_orders(self):
        child_orders = self.create_child_orders()
        action = self.env.ref('mrp.mrp_production_action').read()[0]
        action['domain'] = [('id', 'in', [order.id for order in child_orders])]
        action['context'] = {'create': False}
        return action
