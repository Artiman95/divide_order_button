from odoo import models, fields, api


class MrpProductionChildOrder(models.TransientModel):
    _name = "mrp.production.child.order"
    _description = "MRP Production Child Order"

    wizard_id = fields.Many2one(comodel_name="mrp.production.split.wizard", srting="Wizard")
    child_order_number = fields.Integer(string="Child Order Number", default=1, readonly=True)
    quantity = fields.Integer(string="Quantity of Products")
